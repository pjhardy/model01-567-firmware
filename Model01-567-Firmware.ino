/* Model 01 Keyboard firmware

   Copyright © 2017 Peter Hardy <peter@hardy.dropbear.id.au>.
   See "LICENSE" file for full license details.
*/

#include <Kaleidoscope.h>
#include <Kaleidoscope-MouseKeys.h>
#include <Kaleidoscope-Numlock.h>
#include <Kaleidoscope-LEDControl.h>
#include <Kaleidoscope-LEDEffect-SolidColor.h>

#include "local-keymaps.h"

// This gives a reasonably bright purple backlight for key-seeing
static kaleidoscope::LEDSolidColor purpleBacklight(158, 56, 191);

void setup() {
  Kaleidoscope.use(&LEDControl,
                   &MouseKeys,
                   &NumLock,
                   &purpleBacklight);

  Kaleidoscope.setup();

  NumLock.numPadLayer = NUMPAD;

  // The hardware seems to need a little time to settle. A shorter
  // delay than this keeps the static light activation from working.
  delay(250);
  purpleBacklight.activate();
}

void loop() {
  Kaleidoscope.loop();
}
