Keyboardio Model 01 firmware
============================

A custom firmware for the Keyboardio Model 01. The name comes from the serial
number of my keyboard (#000567). This is not modifed from the default firmware,
but written from scratch using [Kaleidoscope](https://github.com/keyboardio/Kaleidoscope).

For usage, refer to [the Kaleidoscope wiki](https://github.com/keyboardio/Kaleidoscope/wiki/Keyboardio-Model-01-Introduction).

